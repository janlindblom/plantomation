# plantomation

This is where I keep stuff related to my plant automation projects.

### Raspberry Pi

I'm going to try this one: [raspberry-devbox](https://github.com/nickhutchinson/raspberry-devbox).

#### Python

- Pinouts and Python: [Make Magazine](https://makezine.com/projects/tutorial-raspberry-pi-gpio-pins-and-python/)

#### Ruby

TBD

### BBC micro:bit

TBD
